## Welcome
This is my first Ruby on Rails application

#Author
*[Kim Yu Ng](https://twitter.com/)

# Demo
* auth is demo for authentication
* blog is demo for simple blog posting

# Version
* Ruby 2.0
* Rails 4.0

# Gem
* twitter-bootstrap-rails
* devise
* ...

# Styling
* [ Metro UI bootstrap (modded) ](http://talkslab.github.io/metro-bootstrap/)

# Getting Started?
```ruby
bundle install
```
Run bundle install command to install all the required gems

# If interested?
Feel free fork the repo and take a look at my code
